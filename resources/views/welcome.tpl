{extends file='layouts/default.tpl'}
{block name=title}sample{/block}
{block name=contents}
    <h1 class="form-signin-heading">register</h1>
    {{Form::open(['action' => 'RegisterController@confirm', "class" => "form-signin"])}}
        {{Form::text('email', null, ["class" => "form-control", "placeholder"=>"email", "autofocus" => true])}}
        {{Form::password('password', ["class" => "form-control", "placeholder"=>"password"])}}
    {{Form::close()}}
{/block}
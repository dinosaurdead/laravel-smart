<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class HomeController extends Controller
{

    public function index()
    {
        $static_url = VIEW_FOLDER_STATIC;
        return \View::make('index', compact('static_url'));
    }

}

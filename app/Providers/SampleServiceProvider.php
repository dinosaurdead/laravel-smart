<?php
namespace App\Providers;
use Illuminate\Support\ServiceProvider;
class ApplicationServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->registerFilters();
    }
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
    }
    public function registerFilters()
    {
        $smarty = $this->app['view']->getSmarty();
        $smarty->registerFilter('post', [$this, 'add_header_comment']);
    }
    /**
     * @param $tpl_source
     * @param $smarty
     * @return string
     */
    public function add_header_comment($tpl_source, $smarty)
    {
        return "<?php echo \"<!-- Created by Smarty From ServiceProvider! -->\n\"; ?>\n".$tpl_source;
    }
}
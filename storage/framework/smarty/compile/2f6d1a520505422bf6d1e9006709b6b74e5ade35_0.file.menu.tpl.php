<?php
/* Smarty version 3.1.29, created on 2016-07-04 19:48:18
  from "/home/felipe/p/static/trendy.com/default/includes/menu.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_577abd827ac1c1_39605898',
  'file_dependency' => 
  array (
    '2f6d1a520505422bf6d1e9006709b6b74e5ade35' => 
    array (
      0 => '/home/felipe/p/static/trendy.com/default/includes/menu.tpl',
      1 => 1466627545,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_577abd827ac1c1_39605898 ($_smarty_tpl) {
?>
<!--/.row -->
<div class="row">
	<div class="col-xs-12">
		<nav class="navbar navbar-default custom-category-navbar">
			<div class="">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
							data-target="#navbar"
							aria-expanded="false" aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand hidden-md hidden-lg" href="#">00K Shop</a>
				</div>
				<div id="navbar" class="navbar-collapse collapse">
					<ul class="nav navbar-nav menu-top">
						<?php
$_from = $_smarty_tpl->tpl_vars['categories']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_item_0_saved_item = isset($_smarty_tpl->tpl_vars['item']) ? $_smarty_tpl->tpl_vars['item'] : false;
$__foreach_item_0_saved_key = isset($_smarty_tpl->tpl_vars['key']) ? $_smarty_tpl->tpl_vars['key'] : false;
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['item']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
$__foreach_item_0_saved_local_item = $_smarty_tpl->tpl_vars['item'];
?>
							<?php if ($_smarty_tpl->tpl_vars['item']->value->special != 1) {?>
								<?php if (count($_smarty_tpl->tpl_vars['item']->value->childs) > 0) {?>									
									<li class="main-category categories-top dropdown <?php echo $_smarty_tpl->tpl_vars['item']->value->id;?>
">
										<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
categoria/<?php echo $_smarty_tpl->tpl_vars['item']->value->url_amigavel;?>
" class="dropdown-toggle" data-toggle="dropdown" role="button"
										   aria-expanded="false">
											<!--i class="fa fa-arrow-circle-down"></i-->
											<div><?php echo $_smarty_tpl->tpl_vars['item']->value->nome;?>
</div>
										</a>
										<ul class="dropdown-menu" role="menu">
											<?php
$_from = $_smarty_tpl->tpl_vars['item']->value->childs;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_childs_1_saved_item = isset($_smarty_tpl->tpl_vars['childs']) ? $_smarty_tpl->tpl_vars['childs'] : false;
$_smarty_tpl->tpl_vars['childs'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['childs']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['childs']->value) {
$_smarty_tpl->tpl_vars['childs']->_loop = true;
$__foreach_childs_1_saved_local_item = $_smarty_tpl->tpl_vars['childs'];
?>
												<li><a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
categoria/<?php echo $_smarty_tpl->tpl_vars['item']->value->url_amigavel;?>
/<?php echo $_smarty_tpl->tpl_vars['childs']->value->url_amigavel;?>
 "><?php echo $_smarty_tpl->tpl_vars['childs']->value->nome;?>
</a></li>
											<?php
$_smarty_tpl->tpl_vars['childs'] = $__foreach_childs_1_saved_local_item;
}
if ($__foreach_childs_1_saved_item) {
$_smarty_tpl->tpl_vars['childs'] = $__foreach_childs_1_saved_item;
}
?>
										</ul>
									</li>
								<?php } else { ?>
									<li class="main-category categories-top <?php echo $_smarty_tpl->tpl_vars['item']->value->id;?>
">
										<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
categoria/<?php echo $_smarty_tpl->tpl_vars['item']->value->url_amigavel;?>
">
											<!--i class="fa fa-futbol-o"></i-->
											<div><?php echo $_smarty_tpl->tpl_vars['item']->value->nome;?>
</div>
										</a>
									</li>								
								<?php }?>
							<?php }?>
						<?php
$_smarty_tpl->tpl_vars['item'] = $__foreach_item_0_saved_local_item;
}
if ($__foreach_item_0_saved_item) {
$_smarty_tpl->tpl_vars['item'] = $__foreach_item_0_saved_item;
}
if ($__foreach_item_0_saved_key) {
$_smarty_tpl->tpl_vars['key'] = $__foreach_item_0_saved_key;
}
?>						
						<li class="main-category dropdown float-right">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
							   aria-expanded="false">
								<i class="fa fa-arrow-circle-down"></i>

								<div>Todos os Departamentos</div>
							</a>
							<ul class="dropdown-menu" role="menu">
								<?php
$_from = $_smarty_tpl->tpl_vars['categories']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_item_2_saved_item = isset($_smarty_tpl->tpl_vars['item']) ? $_smarty_tpl->tpl_vars['item'] : false;
$__foreach_item_2_saved_key = isset($_smarty_tpl->tpl_vars['key']) ? $_smarty_tpl->tpl_vars['key'] : false;
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['item']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
$__foreach_item_2_saved_local_item = $_smarty_tpl->tpl_vars['item'];
?>
									<li><a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
categoria/1/<?php echo $_smarty_tpl->tpl_vars['item']->value->url_amigavel;?>
"><?php echo $_smarty_tpl->tpl_vars['item']->value->nome;?>
</a></li>
								<?php
$_smarty_tpl->tpl_vars['item'] = $__foreach_item_2_saved_local_item;
}
if ($__foreach_item_2_saved_item) {
$_smarty_tpl->tpl_vars['item'] = $__foreach_item_2_saved_item;
}
if ($__foreach_item_2_saved_key) {
$_smarty_tpl->tpl_vars['key'] = $__foreach_item_2_saved_key;
}
?>
							</ul>
						</li>
					</ul>
				</div>
				<!--/.nav-collapse -->
			</div>
			<!--/.container-fluid -->
		</nav>
	</div>
</div><?php }
}

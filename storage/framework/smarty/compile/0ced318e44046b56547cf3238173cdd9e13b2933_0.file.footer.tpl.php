<?php
/* Smarty version 3.1.29, created on 2016-07-04 19:48:18
  from "/home/felipe/p/static/trendy.com/default/includes/footer.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_577abd82988525_95077053',
  'file_dependency' => 
  array (
    '0ced318e44046b56547cf3238173cdd9e13b2933' => 
    array (
      0 => '/home/felipe/p/static/trendy.com/default/includes/footer.tpl',
      1 => 1467211946,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:includes/newsletter.tpl' => 1,
  ),
),false)) {
function content_577abd82988525_95077053 ($_smarty_tpl) {
?>
<footer class="col-xs-12">
	<div class="container footer-links">
		<div class="row footer-links">
			<?php
$_from = $_smarty_tpl->tpl_vars['links']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_link_0_saved_item = isset($_smarty_tpl->tpl_vars['link']) ? $_smarty_tpl->tpl_vars['link'] : false;
$_smarty_tpl->tpl_vars['link'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['link']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['link']->value) {
$_smarty_tpl->tpl_vars['link']->_loop = true;
$__foreach_link_0_saved_local_item = $_smarty_tpl->tpl_vars['link'];
?>
				<div class="col-xs-12 col-sm-3">
					<h3><?php echo $_smarty_tpl->tpl_vars['link']->value->nome;?>
</h3>
					<ul>
						<?php
$_from = $_smarty_tpl->tpl_vars['link']->value->childs;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_item_1_saved_item = isset($_smarty_tpl->tpl_vars['item']) ? $_smarty_tpl->tpl_vars['item'] : false;
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['item']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
$__foreach_item_1_saved_local_item = $_smarty_tpl->tpl_vars['item'];
?>
							<li><a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
institucional/<?php echo $_smarty_tpl->tpl_vars['item']->value->url_amigavel;?>
"><?php echo $_smarty_tpl->tpl_vars['item']->value->title;?>
</a></li>
						<?php
$_smarty_tpl->tpl_vars['item'] = $__foreach_item_1_saved_local_item;
}
if ($__foreach_item_1_saved_item) {
$_smarty_tpl->tpl_vars['item'] = $__foreach_item_1_saved_item;
}
?>
					</ul>
				</div>
			<?php
$_smarty_tpl->tpl_vars['link'] = $__foreach_link_0_saved_local_item;
}
if ($__foreach_link_0_saved_item) {
$_smarty_tpl->tpl_vars['link'] = $__foreach_link_0_saved_item;
}
?>

			<div class="col-xs-12 col-sm-3">
			    <h3>Newsletter</h3>
			    <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:includes/newsletter.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

			</div>
          
			<div class="col-xs-12 col-sm-3">
				<h3>Facebook</h3>
              <div class="fb-page" data-href="https://www.facebook.com/trendysuplementos/" data-width="260" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/trendysuplementos/"><a href="https://www.facebook.com/trendysuplementos/">Trendy Suplementos</a></blockquote></div></div>

				
			</div>			
			<div class="clearfix"></div>
			<div class="col-xs-6 col-sm-3">
			    <h3>Redes Sociais</h3>

			    <div class="social-icons">
				<a href="#"><i class="fa fa-facebook-square"></i></a>
				<a href="#"><i class="fa fa-twitter-square"></i></a>
				<a href="#"><i class="fa fa-linkedin-square"></i></a>
				<a href="#"><i class="fa fa-instagram"></i></a>
				<a href="#"><i class="fa fa-google-plus-square"></i></a>
				<a href="#"><i class="fa fa-youtube-square"></i></a>
				<a href="#"><i class="fa fa-pinterest-square"></i></a>
			    </div>
			</div>
			<div class="col-xs-6">
				<h3>Formas de pagamento</h3>

				<div class="row">
					<div class="col-sm-5 col-xs-12">
						<div class="payment-section">
							<ul class="payment-icons">
								<li class="sprite-logo-mercadopago"></li>
								
								<li class="sprite-logo-boleto"></li>
								<div class="clearfix"></div>
							</ul>
						</div>
					</div>
				</div>
			</div>
			
			
		</div>
	</div>
	
</footer>
<div class="container">
				<div class="bottom-area">
					<p class="bottom-text">		
						<strong>Endereço: </strong><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['datastore']->value[0]->endereco, ENT_QUOTES, 'UTF-8', true);?>
, <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['datastore']->value[0]->numero, ENT_QUOTES, 'UTF-8', true);?>
 | CEP <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['datastore']->value[0]->cep, ENT_QUOTES, 'UTF-8', true);?>
, <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['datastore']->value[0]->bairro, ENT_QUOTES, 'UTF-8', true);?>
 - <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['datastore']->value[0]->cidade, ENT_QUOTES, 'UTF-8', true);?>
 / <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['datastore']->value[0]->estado, ENT_QUOTES, 'UTF-8', true);?>
.<br>
						<strong>Telefone: </strong><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['datastore']->value[0]->telefone, ENT_QUOTES, 'UTF-8', true);?>
<br>
						<strong>Horário: </strong> 2as às 6as-feiras: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['datastore']->value[0]->segunda_fisico, ENT_QUOTES, 'UTF-8', true);?>
 - Sábados: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['datastore']->value[0]->sabado_online, ENT_QUOTES, 'UTF-8', true);?>
<br>
						<strong>Razão Social: </strong><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['datastore']->value[0]->nome, ENT_QUOTES, 'UTF-8', true);?>
.<br>
						<strong>CNPJ: </strong><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['datastore']->value[0]->cnpj, ENT_QUOTES, 'UTF-8', true);?>
<br>
					</p>

					<div class="dev-signature">
						<a href="http://www.00k.com.br" target="_blank" class="des2"
						   title="00K e-business | tools">00K e-business | tools</a>
					</div>
				</div>
			</div>
                      
                      <div id="fb-root"></div>
<?php echo '<script'; ?>
>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));<?php echo '</script'; ?>
>
                      

                      
                      <!--Start of Zopim Live Chat Script-->
<?php echo '<script'; ?>
 type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="//v2.zopim.com/?3zAxsu8UkUliri5lTekcWmZ2lNhQG5YU";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
<?php echo '</script'; ?>
>
<!--End of Zopim Live Chat Script-->

<?php }
}

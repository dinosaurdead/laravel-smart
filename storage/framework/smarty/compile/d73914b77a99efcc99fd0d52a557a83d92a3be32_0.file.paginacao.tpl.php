<?php
/* Smarty version 3.1.29, created on 2016-07-04 19:48:18
  from "/home/felipe/p/static/trendy.com/default/includes/paginacao.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_577abd828f3c52_94372902',
  'file_dependency' => 
  array (
    'd73914b77a99efcc99fd0d52a557a83d92a3be32' => 
    array (
      0 => '/home/felipe/p/static/trendy.com/default/includes/paginacao.tpl',
      1 => 1466627545,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_577abd828f3c52_94372902 ($_smarty_tpl) {
?>
<nav class="col-md-4" style="width: 100%;">
	<ul class="pagination">
		<?php if ($_smarty_tpl->tpl_vars['paginaAtual']->value != 1) {?>
			<li>
				<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value, ENT_QUOTES, 'UTF-8', true);?>
?pagina=<?php echo $_smarty_tpl->tpl_vars['paginaAtual']->value-1;?>
" aria-label="Previous">
					<span aria-hidden="true">«</span>
				</a>
			</li>
			<!--a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value, ENT_QUOTES, 'UTF-8', true);?>
?pagina=<?php echo $_smarty_tpl->tpl_vars['paginaAtual']->value-1;?>
" class="nav back-button"></a-->
		<?php }?>
			<?php
$_smarty_tpl->tpl_vars['x'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['x']->step = 1;$_smarty_tpl->tpl_vars['x']->total = (int) ceil(($_smarty_tpl->tpl_vars['x']->step > 0 ? $_smarty_tpl->tpl_vars['paginasMostrar']->value+1 - ($_smarty_tpl->tpl_vars['contadorInicial']->value) : $_smarty_tpl->tpl_vars['contadorInicial']->value-($_smarty_tpl->tpl_vars['paginasMostrar']->value)+1)/abs($_smarty_tpl->tpl_vars['x']->step));
if ($_smarty_tpl->tpl_vars['x']->total > 0) {
for ($_smarty_tpl->tpl_vars['x']->value = $_smarty_tpl->tpl_vars['contadorInicial']->value, $_smarty_tpl->tpl_vars['x']->iteration = 1;$_smarty_tpl->tpl_vars['x']->iteration <= $_smarty_tpl->tpl_vars['x']->total;$_smarty_tpl->tpl_vars['x']->value += $_smarty_tpl->tpl_vars['x']->step, $_smarty_tpl->tpl_vars['x']->iteration++) {
$_smarty_tpl->tpl_vars['x']->first = $_smarty_tpl->tpl_vars['x']->iteration == 1;$_smarty_tpl->tpl_vars['x']->last = $_smarty_tpl->tpl_vars['x']->iteration == $_smarty_tpl->tpl_vars['x']->total;?>					
				<?php if ($_smarty_tpl->tpl_vars['paginaAtual']->value == $_smarty_tpl->tpl_vars['x']->value) {?>
					<li class="active"><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value, ENT_QUOTES, 'UTF-8', true);?>
/<?php echo $_smarty_tpl->tpl_vars['x']->value;?>
" class="page-number"><strong><?php echo $_smarty_tpl->tpl_vars['x']->value;?>
</strong></a></li>
				<?php } else { ?>
					<li><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value, ENT_QUOTES, 'UTF-8', true);?>
?pagina=<?php echo $_smarty_tpl->tpl_vars['x']->value;?>
" class="page-number"><?php echo $_smarty_tpl->tpl_vars['x']->value;?>
</a></li>
				<?php }?>				
			<?php }
}
?>

		<?php if ($_smarty_tpl->tpl_vars['qtd_paginas']->value > $_smarty_tpl->tpl_vars['paginaAtual']->value) {?>
			<li>
				<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value, ENT_QUOTES, 'UTF-8', true);?>
?pagina=<?php echo $_smarty_tpl->tpl_vars['paginaAtual']->value+1;?>
" aria-label="Next">
					<span aria-hidden="true">»</span>
				</a>
			</li>
		<?php }?>
	</ul>
</nav><?php }
}

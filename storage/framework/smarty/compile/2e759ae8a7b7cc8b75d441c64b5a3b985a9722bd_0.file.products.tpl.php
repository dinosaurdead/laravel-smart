<?php
/* Smarty version 3.1.29, created on 2016-07-04 19:48:18
  from "/home/felipe/p/static/trendy.com/default/includes/products.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_577abd828a6b73_26625305',
  'file_dependency' => 
  array (
    '2e759ae8a7b7cc8b75d441c64b5a3b985a9722bd' => 
    array (
      0 => '/home/felipe/p/static/trendy.com/default/includes/products.tpl',
      1 => 1466627545,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_577abd828a6b73_26625305 ($_smarty_tpl) {
if (count($_smarty_tpl->tpl_vars['products']->value->items) > 0) {?>
	<?php
$_from = $_smarty_tpl->tpl_vars['products']->value->items;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_item_0_saved_item = isset($_smarty_tpl->tpl_vars['item']) ? $_smarty_tpl->tpl_vars['item'] : false;
$__foreach_item_0_saved_key = isset($_smarty_tpl->tpl_vars['key']) ? $_smarty_tpl->tpl_vars['key'] : false;
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['item']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
$__foreach_item_0_saved_local_item = $_smarty_tpl->tpl_vars['item'];
?>
		<!-- ./Thumb colum -->
		<div class="col-sm-6 col-md-4 col-lg-3 gallery-grid">
			<div class="thumbnail">
			<?php if ($_smarty_tpl->tpl_vars['item']->value->type == 'simple') {?>
				<?php $_smarty_tpl->tpl_vars["url_type"] = new Smarty_Variable("produto", null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "url_type", 0);?>
			<?php } else { ?>
				<?php if ($_smarty_tpl->tpl_vars['item']->value->type == 'group') {?>
					<?php $_smarty_tpl->tpl_vars["url_type"] = new Smarty_Variable("grupo", null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "url_type", 0);?>
				<?php } else { ?>
					<?php $_smarty_tpl->tpl_vars["url_type"] = new Smarty_Variable("kit", null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "url_type", 0);?>
				<?php }?>
			<?php }?>		
				<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;
echo htmlspecialchars($_smarty_tpl->tpl_vars['url_type']->value, ENT_QUOTES, 'UTF-8', true);?>
/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value->categories[0]->slug, ENT_QUOTES, 'UTF-8', true);?>
/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value->seo->url, ENT_QUOTES, 'UTF-8', true);?>
/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value->brand->info->name, ENT_QUOTES, 'UTF-8', true);?>
">
                   <?php if ($_smarty_tpl->tpl_vars['item']->value->info->itemFlag) {?>
               <div class="flag <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value->info->itemFlag, ENT_QUOTES, 'UTF-8', true);?>
"><h5><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value->info->itemFlag, ENT_QUOTES, 'UTF-8', true);?>
</h5></div>
               <?php }?>
					<?php if (count($_smarty_tpl->tpl_vars['item']->value->images) > 0 && $_smarty_tpl->tpl_vars['item']->value->images[0]->path->large != '') {?>
						<?php if (strstr($_smarty_tpl->tpl_vars['item']->value->images[0]->path->large,"https")) {?> 
							<img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value->images[0]->path->large, ENT_QUOTES, 'UTF-8', true);?>
" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value->info->name, ENT_QUOTES, 'UTF-8', true);?>
">
						<?php } else { ?>
							<img src="https://00k.com.br/img/<?php echo $_smarty_tpl->tpl_vars['datastore']->value[0]->url_fan_page;?>
/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value->images[0]->path->large, ENT_QUOTES, 'UTF-8', true);?>
" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value->info->name, ENT_QUOTES, 'UTF-8', true);?>
">
						<?php }?>
					<?php } else { ?>
						<img src="<?php echo $_smarty_tpl->tpl_vars['static_url']->value;?>
img/product-bernard-cornwell-graal-box-01.jpg" alt="">
					<?php }?>				
				</a>

				<div class="caption">
					<h3><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value->info->name, ENT_QUOTES, 'UTF-8', true);?>
</h3>
					<h4><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value->info->subtitle, ENT_QUOTES, 'UTF-8', true);?>
</h4>

					<div class="price">
						<div class="full-price">De: <span class="dashed">R$ <?php echo number_format($_smarty_tpl->tpl_vars['item']->value->price[0]->value,2,",",".");?>
</span></div>
						<div class="sale-price"><span class="for">Por: </span><span class="green">R$<?php echo number_format($_smarty_tpl->tpl_vars['item']->value->price[0]->value,2,",",".");?>
</span></div>
						
					</div>
					<p>
						<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;
echo htmlspecialchars($_smarty_tpl->tpl_vars['url_type']->value, ENT_QUOTES, 'UTF-8', true);?>
/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value->categories[0]->slug, ENT_QUOTES, 'UTF-8', true);?>
/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value->seo->url, ENT_QUOTES, 'UTF-8', true);?>
/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value->brand->info->name, ENT_QUOTES, 'UTF-8', true);?>
" class="btn btn-success buy-button" role="button">Comprar</a>
					</p>
				</div>
				<!-- /.caption -->
			</div>
		</div>
		<!-- /.Thumb colum -->
	<?php
$_smarty_tpl->tpl_vars['item'] = $__foreach_item_0_saved_local_item;
}
if ($__foreach_item_0_saved_item) {
$_smarty_tpl->tpl_vars['item'] = $__foreach_item_0_saved_item;
}
if ($__foreach_item_0_saved_key) {
$_smarty_tpl->tpl_vars['key'] = $__foreach_item_0_saved_key;
}
} else { ?>
	<div class="col-sm-6 col-md-4 col-lg-3 gallery-grid">
		<p>Nenhum produto encontrado</p>
	</div>
<?php }
}
}

<?php
/* Smarty version 3.1.29, created on 2016-07-01 20:41:51
  from "/home/felipe/p/static/trendy.local/default/includes/header.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5776d58fb23db4_40130339',
  'file_dependency' => 
  array (
    'f1ec4156d593581660ea8d88a6c2e3a77c9998bb' => 
    array (
      0 => '/home/felipe/p/static/trendy.local/default/includes/header.tpl',
      1 => 1466627545,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5776d58fb23db4_40130339 ($_smarty_tpl) {
echo '<script'; ?>
 src="https://api.prognoos.com/static/e.js/00k.com.br/"><?php echo '</script'; ?>
>

<div class="row">
	<div class="col-md-3">
		<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['base_url']->value, ENT_QUOTES, 'UTF-8', true);?>
"><img class="header-logo" src="<?php echo $_smarty_tpl->tpl_vars['static_url']->value;?>
images/o_1a3uggl6t1leg1hs8rdgu45haja.png" alt="Logo Trendy"></a>
	</div>
	<div class="col-xs-12 col-md-9">
		<div class="row">
		<span class="col-xs-6 margin-top-8">
		  Olá <strong>visitante</strong>,<span class="hidden-xs hidden-sm"> seja bem vindo!</span> <a
				target="_blank" href="<?php echo $_smarty_tpl->tpl_vars['links2']->value[0]->order_trace_url;?>
">Entre</a> ou <a href="<?php echo $_smarty_tpl->tpl_vars['links2']->value[0]->account_url;?>
" target="_blank">Cadastre-se</a>
		</span>
			<nav class="col-xs-6 custom-user-menu">
				<ul>
					<li><a href="<?php echo $_smarty_tpl->tpl_vars['links2']->value[0]->account_url;?>
">Minha Conta</a></li>
					<li><a href="<?php echo $_smarty_tpl->tpl_vars['links2']->value[0]->order_trace_url;?>
">Meus Pedidos</a></li>
					<li><a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
pagina/<?php echo $_smarty_tpl->tpl_vars['treatment']->value[0]->url_amigavel;?>
">Atendimento</a></li>
				</ul>
			</nav>
		</div>
		<div class="search-cart-box">
			<div class="cart-box-container">
				<div class="col-xs-6 col-sm-9">
					<div class="input-group custom-search-field">
						<form id="search" method="get" action="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['base_url']->value, ENT_QUOTES, 'UTF-8', true);?>
busca">
							<input type="text" name="busca" class="form-control" placeholder="Digite aqui o que você procura...">
                              <span class="input-group-btn">
								<button  type="submit"  class="btn btn-default" type="button">
									<span class="glyphicon glyphicon-search" aria-hidden="true"></span>
								</button>
								</span><!-- /.input-group-btn -->
						</form>
						
					</div>
					<!-- /.input-group -->
				</div>
				<!-- /.col-xs-8 -->
				<div class="col-xs-6 col-sm-3">
					<div class="shopping-cart-box">
						<span class="glyphicon glyphicon-shopping-cart custom-shopping-cart"
							  aria-hidden="true"></span>

						<div class="pull-left">
							<div><a href="https://00k.com.br/trendysuplementos/carrinho">Meu Carrinho</a></div>
							<div><a href="https://00k.com.br/trendysuplementos/carrinho">R$ 00,00</a></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--/.col-xs-12 -->
</div>
<!--/.row --><?php }
}

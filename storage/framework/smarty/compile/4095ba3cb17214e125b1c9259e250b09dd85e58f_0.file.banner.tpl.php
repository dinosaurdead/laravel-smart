<?php
/* Smarty version 3.1.29, created on 2016-07-21 21:23:14
  from "/home/felipe/p/static/artmilhas.local/default/includes/banner.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_57913d42469e98_90790973',
  'file_dependency' => 
  array (
    '4095ba3cb17214e125b1c9259e250b09dd85e58f' => 
    array (
      0 => '/home/felipe/p/static/artmilhas.local/default/includes/banner.tpl',
      1 => 1466690842,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_57913d42469e98_90790973 ($_smarty_tpl) {
?>
<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

	<!-- Wrapper for slides -->
	<div class="carousel-inner hidden-sm hidden-xs" role="listbox">	
		<?php
$_from = $_smarty_tpl->tpl_vars['banners']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_item_0_saved_item = isset($_smarty_tpl->tpl_vars['item']) ? $_smarty_tpl->tpl_vars['item'] : false;
$__foreach_item_0_saved_key = isset($_smarty_tpl->tpl_vars['key']) ? $_smarty_tpl->tpl_vars['key'] : false;
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['item']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
$__foreach_item_0_saved_local_item = $_smarty_tpl->tpl_vars['item'];
?>
			<div class="item <?php if ($_smarty_tpl->tpl_vars['key']->value == 0) {?> active <?php }?>">
				<?php if ($_smarty_tpl->tpl_vars['item']->value->image_link != '') {?>
					<a href="<?php echo $_smarty_tpl->tpl_vars['item']->value->image_link;?>
" target="_blank">
				<?php }?>
					<?php if (substr($_smarty_tpl->tpl_vars['item']->value->file_name,0,4) == 'http') {?>
						<img src="<?php echo $_smarty_tpl->tpl_vars['item']->value->file_name;?>
" alt="<?php echo $_smarty_tpl->tpl_vars['item']->value->file_name;?>
">
					<?php } else { ?>
						<img src="https://00k.com.br/web/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['datastore']->value[0]->url_fan_page, ENT_QUOTES, 'UTF-8', true);?>
/images_sys/<?php echo $_smarty_tpl->tpl_vars['item']->value->file_name;?>
" alt="<?php echo $_smarty_tpl->tpl_vars['item']->value->file_name;?>
">
					<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['item']->value->image_link != '') {?>
					</a>
				<?php }?>
				<div class="carousel-caption">
				</div>
			</div>		
		<?php
$_smarty_tpl->tpl_vars['item'] = $__foreach_item_0_saved_local_item;
}
if ($__foreach_item_0_saved_item) {
$_smarty_tpl->tpl_vars['item'] = $__foreach_item_0_saved_item;
}
if ($__foreach_item_0_saved_key) {
$_smarty_tpl->tpl_vars['key'] = $__foreach_item_0_saved_key;
}
?>
	</div>

	<!-- Controls -->
	<a class="left carousel-control hidden-sm hidden-xs" href="#carousel-example-generic" role="button"
	   data-slide="prev">
		<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
		<span class="sr-only">Previous</span>
	</a>
	<a class="right carousel-control hidden-sm hidden-xs" href="#carousel-example-generic" role="button"
	   data-slide="next">
		<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
		<span class="sr-only">Next</span>
	</a>
</div><?php }
}

<?php
/* Smarty version 3.1.29, created on 2016-07-21 21:23:14
  from "/home/felipe/p/static/artmilhas.local/default/includes/header.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_57913d423d0971_10580459',
  'file_dependency' => 
  array (
    'bd66723a7ec32dd1e1fd034498148ca861c64479' => 
    array (
      0 => '/home/felipe/p/static/artmilhas.local/default/includes/header.tpl',
      1 => 1466779366,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_57913d423d0971_10580459 ($_smarty_tpl) {
?>
<div class="row">
	<div class="col-md-3">
		<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['base_url']->value, ENT_QUOTES, 'UTF-8', true);?>
"><img class="header-logo" src="http://www.static.artmilhas.com.br/www.artmilhas.com.br/default/images/logo.png" alt="Artmilhas"></a>
	</div>
	<div class="col-xs-12 col-md-9">
		<div class="row">
		<span class="col-xs-6 margin-top-8">
		  Olá <strong>visitante</strong>,<span class="hidden-xs hidden-sm"> seja bem vindo!</span> <a
				target="_blank" href="<?php echo $_smarty_tpl->tpl_vars['links2']->value[0]->order_trace_url;?>
">Entre</a> ou <a href="<?php echo $_smarty_tpl->tpl_vars['links2']->value[0]->account_url;?>
" target="_blank">Cadastre-se</a>
		</span>
			<nav class="col-xs-6 custom-user-menu">
				<ul>
					<li><a href="https://00k.com.br/<?php echo $_smarty_tpl->tpl_vars['datastore']->value[0]->loja_id;?>
/1/1/redir">Minha Conta</a></li>
					<li><a href="<?php echo $_smarty_tpl->tpl_vars['links2']->value[0]->order_trace_url;?>
">Meus Pedidos</a></li>
					<li><a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
institucional/fale-conosco">Atendimento</a></li>
				</ul>
			</nav>
		</div>
		<div class="search-cart-box">
			<div class="cart-box-container">
				<div class="col-xs-12 col-sm-12 col-md-9">
					<div class="input-group custom-search-field">
						<form id="search" method="get" action="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['base_url']->value, ENT_QUOTES, 'UTF-8', true);?>
busca">
							<input type="text" name="busca" class="form-control" placeholder="Digite aqui o que você procura...">
                              <span class="input-group-btn">
								<button  type="submit"  class="btn btn-default" type="button">
									<span class="glyphicon glyphicon-search" aria-hidden="true"></span>
								</button>
								</span><!-- /.input-group-btn -->
						</form>
						
					</div>
					<!-- /.input-group -->
				</div>
				<!-- /.col-xs-8 -->
				<div class="col-xs-6 col-sm-3">
					<div class="shopping-cart-box hidden-xs hidden-sm">
						<span class="glyphicon glyphicon-shopping-cart custom-shopping-cart"
							  aria-hidden="true"></span>

						<div class="pull-left">
							<div><a href="https://00k.com.br/<?php echo $_smarty_tpl->tpl_vars['datastore']->value[0]->url_fan_page;?>
/carrinho">Meu Carrinho</a></div>
							<div><a href="https://00k.com.br/<?php echo $_smarty_tpl->tpl_vars['datastore']->value[0]->url_fan_page;?>
/carrinho">R$ 00,00</a></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--/.col-xs-12 -->
</div>
<!--/.row --><?php }
}

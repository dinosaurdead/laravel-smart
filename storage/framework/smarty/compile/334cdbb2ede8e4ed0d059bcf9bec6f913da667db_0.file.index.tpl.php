<?php
/* Smarty version 3.1.29, created on 2016-07-04 19:48:18
  from "/home/felipe/p/static/trendy.com/default/index.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_577abd826f4964_40692998',
  'file_dependency' => 
  array (
    '334cdbb2ede8e4ed0d059bcf9bec6f913da667db' => 
    array (
      0 => '/home/felipe/p/static/trendy.com/default/index.tpl',
      1 => 1466627545,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:includes/header.tpl' => 1,
    'file:includes/menu.tpl' => 1,
    'file:includes/banner.tpl' => 1,
    'file:includes/marks.tpl' => 1,
    'file:includes/products.tpl' => 1,
    'file:includes/paginacao.tpl' => 1,
    'file:includes/footer.tpl' => 1,
  ),
),false)) {
function content_577abd826f4964_40692998 ($_smarty_tpl) {
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Trendy Suplementos</title>

           <!-- Capturar tipo de página -->
    <meta type="page" content="<?php echo $_smarty_tpl->tpl_vars['base_url_full']->value;?>
" />
		<!-- Bootstrap -->
		<link href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['static_url']->value, ENT_QUOTES, 'UTF-8', true);?>
css/bootstrap.min.css" rel="stylesheet">
		<link href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['static_url']->value, ENT_QUOTES, 'UTF-8', true);?>
css/custom.css" rel="stylesheet">
		<link rel="stylesheet" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['static_url']->value, ENT_QUOTES, 'UTF-8', true);?>
font-awesome-4.2.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['static_url']->value, ENT_QUOTES, 'UTF-8', true);?>
css/utilcarousel/util.carousel.css">
		<!-- UtilCarousel base css and default theme for control. -->
		<link rel="stylesheet" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['static_url']->value, ENT_QUOTES, 'UTF-8', true);?>
css/utilcarousel/util.carousel.skins.css">
       


		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- ATENÇÃO: Respond.js não funciona quando acessado via arquivo (file://) -->
		<!--[if lt IE 9]>
		<?php echo '<script'; ?>
 src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"><?php echo '</script'; ?>
>
		<?php echo '<script'; ?>
 src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"><?php echo '</script'; ?>
>
		<![endif]-->
	</head>

	<body>
		

<div class="container">
			<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:includes/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

			<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:includes/menu.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
		
			
			<div class="row row-offcanvas row-offcanvas-right">
				<div class="col-xs-12">
					<p class="pull-right visible-xs hidden-sm hidden-xs">
					<button type="button" class="btn btn-success buy-button btn-xs" data-toggle="offcanvas">Toggle nav
					</button>
					</p>
					<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:includes/banner.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                  
                  
                  <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:includes/marks.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

					<div class="row">
						<div class="custom-product-showcase">
							<div class="sidebar-product-showcase hidden-xs hidden-sm">
								<div class="col-sm-2 col-md-3 col-lg-2">									
									<?php
$_from = $_smarty_tpl->tpl_vars['categories']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_item_0_saved_item = isset($_smarty_tpl->tpl_vars['item']) ? $_smarty_tpl->tpl_vars['item'] : false;
$__foreach_item_0_saved_key = isset($_smarty_tpl->tpl_vars['key']) ? $_smarty_tpl->tpl_vars['key'] : false;
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['item']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
$__foreach_item_0_saved_local_item = $_smarty_tpl->tpl_vars['item'];
?>
										<div class="list-group">
											<div class="list-group-item active">
												<h4 class="list-group-item-heading">
													<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
categoria/<?php echo $_smarty_tpl->tpl_vars['item']->value->url_amigavel;?>
"><?php echo $_smarty_tpl->tpl_vars['item']->value->nome;?>
</a>
													<?php if (count($_smarty_tpl->tpl_vars['item']->value->childs) > 0) {?>
														<a data-toggle="collapse" class="menuCollapse collapsed" href="#<?php echo $_smarty_tpl->tpl_vars['item']->value->url_amigavel;?>
" aria-expanded="true"
														   aria-controls="<?php echo $_smarty_tpl->tpl_vars['item']->value->url_amigavel;?>
">
															<i class="fa fa-plus-circle"></i>
<i class="fa fa-minus-circle" style="float: right;"></i>
														</a>
													<?php }?>
												</h4>

												<div class="clearfix"></div>
											</div>
											<?php if (count($_smarty_tpl->tpl_vars['item']->value->childs) > 0) {?>
												<div class="collapse" id="<?php echo $_smarty_tpl->tpl_vars['item']->value->url_amigavel;?>
">
													<?php
$_from = $_smarty_tpl->tpl_vars['item']->value->childs;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_childs_1_saved_item = isset($_smarty_tpl->tpl_vars['childs']) ? $_smarty_tpl->tpl_vars['childs'] : false;
$_smarty_tpl->tpl_vars['childs'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['childs']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['childs']->value) {
$_smarty_tpl->tpl_vars['childs']->_loop = true;
$__foreach_childs_1_saved_local_item = $_smarty_tpl->tpl_vars['childs'];
?>
														<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
categoria/<?php echo $_smarty_tpl->tpl_vars['item']->value->url_amigavel;?>
/<?php echo $_smarty_tpl->tpl_vars['childs']->value->url_amigavel;?>
" class="list-group-item"><?php echo $_smarty_tpl->tpl_vars['childs']->value->nome;?>
</a>
													<?php
$_smarty_tpl->tpl_vars['childs'] = $__foreach_childs_1_saved_local_item;
}
if ($__foreach_childs_1_saved_item) {
$_smarty_tpl->tpl_vars['childs'] = $__foreach_childs_1_saved_item;
}
?>
												</div>
											<?php }?>
										</div>
									<?php
$_smarty_tpl->tpl_vars['item'] = $__foreach_item_0_saved_local_item;
}
if ($__foreach_item_0_saved_item) {
$_smarty_tpl->tpl_vars['item'] = $__foreach_item_0_saved_item;
}
if ($__foreach_item_0_saved_key) {
$_smarty_tpl->tpl_vars['key'] = $__foreach_item_0_saved_key;
}
?>								
								</div>
							</div>
							<div class="col-xs-12 col-md-9 col-lg-10">
								<div class="row product-listing">
									<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:includes/products.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
	
								</div>
								<!--/row-->
								<div class="col-md-4"></div>
								<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:includes/paginacao.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

								<div class="col-md-4"></div>
							</div>
							<!-- /.col-xs-12 -->
						</div>
					<!-- /.custom-product-showcase -->
					</div>
				<!--/row-->
				</div>			
			</div>			

          
					
		</div>
      
      
			<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:includes/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

			
		<!--/.container-->
		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<?php echo '<script'; ?>
 src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"><?php echo '</script'; ?>
>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<?php echo '<script'; ?>
 src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['static_url']->value, ENT_QUOTES, 'UTF-8', true);?>
js/bootstrap.js"><?php echo '</script'; ?>
>
		<?php echo '<script'; ?>
 src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['static_url']->value, ENT_QUOTES, 'UTF-8', true);?>
js/utilcarousel/jquery.utilcarousel.min.js"><?php echo '</script'; ?>
>
		<!-- Include utilcarousel js file -->
		<?php echo '<script'; ?>
 type="text/javascript">
			$('.normal-imglist').utilCarousel({
				pagination: false,
				navigationText: ['<i class="icon-left-open-big"></i>', '<i class=" icon-right-open-big"></i>'],
				navigation: true,
				showItems: 7,
				lazyLoad: true,
				itemAnimation: true
			});
			// Comentei esse código por que ele está dando erro e não será utilizado atualmente. Ele destaca o Carousel
			// $('.normal-imglist').highlightsCarousel({
			//   pagination : false,
			//   navigationText : ['<i class="icon-left-open-big"></i>', '<i class=" icon-right-open-big"></i>'],
			//   navigation : true,
			//   showItems : 2,
			//   lazyLoad : true,
			//   itemAnimation : true
			// });
		<?php echo '</script'; ?>
>
		<!-- /.Showcase das imagens de produtos -->
		<?php echo '<script'; ?>
 type="text/javascript">
			$('ul.dropdown-menu [data-toggle=dropdown]').on('click', function (event) {
				// Avoid following the href location when clicking
				event.preventDefault();
				// Avoid having the menu to close when clicking
				event.stopPropagation();
				// If a menu is already open we close it
				$('ul.dropdown-menu [data-toggle=dropdown]').parent().removeClass('open');
				// opening the one you clicked on
				$(this).parent().addClass('open');
			});
		<?php echo '</script'; ?>
>
	</body>
</html><?php }
}

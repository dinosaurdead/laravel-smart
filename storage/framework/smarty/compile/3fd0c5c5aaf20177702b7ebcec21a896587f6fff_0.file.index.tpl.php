<?php
/* Smarty version 3.1.29, created on 2016-07-21 21:23:14
  from "/home/felipe/p/static/artmilhas.local/default/index.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_57913d423209d9_22394540',
  'file_dependency' => 
  array (
    '3fd0c5c5aaf20177702b7ebcec21a896587f6fff' => 
    array (
      0 => '/home/felipe/p/static/artmilhas.local/default/index.tpl',
      1 => 1467232173,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:includes/analytics.tpl' => 1,
    'file:includes/header.tpl' => 1,
    'file:includes/menu.tpl' => 1,
    'file:includes/banner.tpl' => 1,
    'file:includes/productsGet.tpl' => 1,
    'file:includes/footer.tpl' => 1,
  ),
),false)) {
function content_57913d423209d9_22394540 ($_smarty_tpl) {
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="title" content="<?php echo $_smarty_tpl->tpl_vars['datastore']->value[0]->slogan;?>
">
		<meta name="description" content="<?php echo $_smarty_tpl->tpl_vars['datastore']->value[0]->meta_description;?>
">
		<meta name="keywords" content="<?php echo $_smarty_tpl->tpl_vars['datastore']->value[0]->meta_keywords;?>
">
		<meta name="author" content="'00k e-commerce tools - https://00k.com.br/'">
		<title>Artmilhas</title>

		<!-- Bootstrap -->
		<link href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['static_url']->value, ENT_QUOTES, 'UTF-8', true);?>
css/bootstrap.min.css" rel="stylesheet">
		<link href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['static_url']->value, ENT_QUOTES, 'UTF-8', true);?>
css/custom.css" rel="stylesheet">
		<link rel="stylesheet" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['static_url']->value, ENT_QUOTES, 'UTF-8', true);?>
font-awesome-4.2.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['static_url']->value, ENT_QUOTES, 'UTF-8', true);?>
css/utilcarousel/util.carousel.css">
		<!-- UtilCarousel base css and default theme for control. -->
		<link rel="stylesheet" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['static_url']->value, ENT_QUOTES, 'UTF-8', true);?>
css/utilcarousel/util.carousel.skins.css">
                        <link rel="icon" type="image/ico" href="https://00k.com.br/web/artmilhas/images_sys/galeria/favicon.ico" />

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- ATENÇÃO: Respond.js não funciona quando acessado via arquivo (file://) -->
		<!--[if lt IE 9]>
		<?php echo '<script'; ?>
 src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"><?php echo '</script'; ?>
>
		<?php echo '<script'; ?>
 src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"><?php echo '</script'; ?>
>
		<![endif]-->

			<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:includes/analytics.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	</head>

	<body>


	<!--/.container-->
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<?php echo '<script'; ?>
 src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"><?php echo '</script'; ?>
>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<?php echo '<script'; ?>
 src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['static_url']->value, ENT_QUOTES, 'UTF-8', true);?>
js/bootstrap.js"><?php echo '</script'; ?>
>
	<?php echo '<script'; ?>
 src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['static_url']->value, ENT_QUOTES, 'UTF-8', true);?>
js/utilcarousel/jquery.utilcarousel.min.js"><?php echo '</script'; ?>
>
	<!-- Include utilcarousel js file -->
   <div class="row topo no-margin">
    <div class="container color-white">
      <span class="fones"><i class="fa fa-phone-square"></i> </span>
      <span class="email-top"><i class="fa fa-envelope"></i> </span>
		<div class="menutop">
          <nav class="custom-user-menu pull-right no-margin">
				<ul>
					<li><i class="fa fa-list"></i><a class="color-white" href="<?php echo $_smarty_tpl->tpl_vars['links2']->value[0]->order_trace_url;?>
" style="font-size: 13px;"> Meus Pedidos</a></li>
					<li><i class="fa fa-check-circle-o"></i> <a class="color-white" href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
pagina/<?php echo $_smarty_tpl->tpl_vars['treatment']->value[0]->url_amigavel;?>
" style="font-size: 13px;">Atendimento</a></li>
				</ul>
			</nav>
         </div>
      </div>
      </div>
		<div class="container">
        <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:includes/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

        <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:includes/menu.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

		<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:includes/banner.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

			<div class="row row-offcanvas row-offcanvas-right">
				<div class="col-xs-12">
					<div class="row">
						<div class="custom-product-showcase">
							<div class="sidebar-product-showcase">
								<div class="col-sm-2 col-md-3 col-lg-2 category_right hidden-xs hidden-sm">
									<?php
$_from = $_smarty_tpl->tpl_vars['categories']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_item_0_saved_item = isset($_smarty_tpl->tpl_vars['item']) ? $_smarty_tpl->tpl_vars['item'] : false;
$__foreach_item_0_saved_key = isset($_smarty_tpl->tpl_vars['key']) ? $_smarty_tpl->tpl_vars['key'] : false;
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['item']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
$__foreach_item_0_saved_local_item = $_smarty_tpl->tpl_vars['item'];
?>
										<div class="list-group">
											<div class="list-group-item active">
												<h4 class="list-group-item-heading">
													<a style="font-size: 13px;" href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
categoria/<?php echo $_smarty_tpl->tpl_vars['item']->value->url_amigavel;?>
"><?php echo $_smarty_tpl->tpl_vars['item']->value->nome;?>
</a>
													<?php if (count($_smarty_tpl->tpl_vars['item']->value->childs) > 0) {?>
														<a data-toggle="collapse" class="menuCollapse collapsed" href="#<?php echo $_smarty_tpl->tpl_vars['item']->value->url_amigavel;?>
" aria-expanded="true" aria-controls="<?php echo $_smarty_tpl->tpl_vars['item']->value->url_amigavel;?>
">
					<i class="fa fa-plus-circle"></i> <i class="fa fa-minus-circle" style="float: right;"></i>
														</a>
													<?php }?>
												</h4>

												<div class="clearfix"></div>
											</div>
											<?php if (count($_smarty_tpl->tpl_vars['item']->value->childs) > 0) {?>
												<div class="collapse in" id="<?php echo $_smarty_tpl->tpl_vars['item']->value->url_amigavel;?>
">
													<?php
$_from = $_smarty_tpl->tpl_vars['item']->value->childs;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_childs_1_saved_item = isset($_smarty_tpl->tpl_vars['childs']) ? $_smarty_tpl->tpl_vars['childs'] : false;
$_smarty_tpl->tpl_vars['childs'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['childs']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['childs']->value) {
$_smarty_tpl->tpl_vars['childs']->_loop = true;
$__foreach_childs_1_saved_local_item = $_smarty_tpl->tpl_vars['childs'];
?>
														<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
categoria/<?php echo $_smarty_tpl->tpl_vars['item']->value->url_amigavel;?>
/<?php echo $_smarty_tpl->tpl_vars['childs']->value->url_amigavel;?>
" class="list-group-item"><?php echo $_smarty_tpl->tpl_vars['childs']->value->nome;?>
</a>
													<?php
$_smarty_tpl->tpl_vars['childs'] = $__foreach_childs_1_saved_local_item;
}
if ($__foreach_childs_1_saved_item) {
$_smarty_tpl->tpl_vars['childs'] = $__foreach_childs_1_saved_item;
}
?>
												</div>
											<?php }?>
										</div>
									<?php
$_smarty_tpl->tpl_vars['item'] = $__foreach_item_0_saved_local_item;
}
if ($__foreach_item_0_saved_item) {
$_smarty_tpl->tpl_vars['item'] = $__foreach_item_0_saved_item;
}
if ($__foreach_item_0_saved_key) {
$_smarty_tpl->tpl_vars['key'] = $__foreach_item_0_saved_key;
}
?>
								</div>
							<div class="col-xs-12 col-md-9 col-lg-10">
                                  <h2 style="margin-top: 0px !important;" class="">Destaques</h2>
								<div class="row product-listing">
									<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:includes/productsGet.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('div'=>"product-listing",'total'=>"16",'category'=>"1"), 0, false);
?>

								</div>
								<div class="col-md-4"></div>
							</div>
							</div>
                          <div class="clearfix"></div>

						</div>
					<!-- /custom-product-showcase -->
					</div>
				<!--/row-->
				</div>
			</div>

		</div>
			<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:includes/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


 <?php echo '<script'; ?>
 type="text/javascript">
    (function () {
        var yvs = document.createElement("script");
        yvs.type = "text/javascript";
        yvs.async = true;
        yvs.id = "_yvsrc";
        yvs.src = "//service.yourviews.com.br/script/d7155422-f695-4097-926c-fa7c46470c21/yvapi.js";
        var yvs_script = document.getElementsByTagName("script")[0];
        yvs_script.parentNode.insertBefore(yvs, yvs_script);
    })();
<?php echo '</script'; ?>
>

		<?php echo '<script'; ?>
 type="text/javascript">
			$('.normal-imglist').utilCarousel({
				pagination: false,
				navigationText: ['<i class="icon-left-open-big"></i>', '<i class=" icon-right-open-big"></i>'],
				navigation: true,
				showItems: 7,
				lazyLoad: true,
				itemAnimation: true
			});
			// Comentei esse código por que ele está dando erro e não será utilizado atualmente. Ele destaca o Carousel
			// $('.normal-imglist').highlightsCarousel({
			//   pagination : false,
			//   navigationText : ['<i class="icon-left-open-big"></i>', '<i class=" icon-right-open-big"></i>'],
			//   navigation : true,
			//   showItems : 2,
			//   lazyLoad : true,
			//   itemAnimation : true
			// });
		<?php echo '</script'; ?>
>
		<!-- /.Showcase das imagens de produtos -->
		<?php echo '<script'; ?>
 type="text/javascript">
			$('ul.dropdown-menu [data-toggle=dropdown]').on('click', function (event) {
				// Avoid following the href location when clicking
				event.preventDefault();
				// Avoid having the menu to close when clicking
				event.stopPropagation();
				// If a menu is already open we close it
				$('ul.dropdown-menu [data-toggle=dropdown]').parent().removeClass('open');
				// opening the one you clicked on
				$(this).parent().addClass('open');
			});
		<?php echo '</script'; ?>
>
                                  <?php echo '<script'; ?>
>
      $(document).ready(function() {
          	$('#develop').modal('show');

		$('#BOX').modal('show');


             $(".js-calculate-economy").each(function () {
        	var self = this,
        		list = parseFloat($("input[name=list]", self).val().replace(",", ".")),
        		value = parseFloat($("input[name=value]", self).val().replace(",", ".")),
        		$total = $(".js-calculate-economy-total", self);

        	var total = (list-value);
                if(parseInt(total) <= 0 ){
                	$(self).hide();
                } else {
                    $total.append(total.toFixed(2).replace(".", ","));
            	}
       		 });
      });
    <?php echo '</script'; ?>
>
   </body>
</html>
<?php }
}

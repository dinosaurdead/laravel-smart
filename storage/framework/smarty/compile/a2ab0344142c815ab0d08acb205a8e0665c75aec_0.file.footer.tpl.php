<?php
/* Smarty version 3.1.29, created on 2016-07-21 21:23:14
  from "/home/felipe/p/static/artmilhas.local/default/includes/footer.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_57913d424c4a57_98335391',
  'file_dependency' => 
  array (
    'a2ab0344142c815ab0d08acb205a8e0665c75aec' => 
    array (
      0 => '/home/felipe/p/static/artmilhas.local/default/includes/footer.tpl',
      1 => 1466690842,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_57913d424c4a57_98335391 ($_smarty_tpl) {
?>
<div class="container">
  <footer>
  <div class="footer-box">
      <div class="row">
      <!-- ABRE ESQUERDA -->
      <div class="col-md-8 col-sm-12 padding-left-10">
          <!-- ABRE SUP ESQUERDA --> 
          <div class="row footer-links">
              <div class="col-md-8 col-sm-8">
              <?php
$_from = $_smarty_tpl->tpl_vars['links']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_link_0_saved_item = isset($_smarty_tpl->tpl_vars['link']) ? $_smarty_tpl->tpl_vars['link'] : false;
$_smarty_tpl->tpl_vars['link'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['link']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['link']->value) {
$_smarty_tpl->tpl_vars['link']->_loop = true;
$__foreach_link_0_saved_local_item = $_smarty_tpl->tpl_vars['link'];
?>
                  <!-- ABRE INFO E SUPORTE -->
                  <div class="col-md-6 col-sm-6 padding-left-10">
                      <h3><?php echo $_smarty_tpl->tpl_vars['link']->value->nome;?>
</h3>
                      <ul>
                          <?php
$_from = $_smarty_tpl->tpl_vars['link']->value->childs;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_item_1_saved_item = isset($_smarty_tpl->tpl_vars['item']) ? $_smarty_tpl->tpl_vars['item'] : false;
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['item']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
$__foreach_item_1_saved_local_item = $_smarty_tpl->tpl_vars['item'];
?>
                              <li><a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
institucional/<?php echo $_smarty_tpl->tpl_vars['item']->value->url_amigavel;?>
"><?php echo $_smarty_tpl->tpl_vars['item']->value->title;?>
</a></li>
                          <?php
$_smarty_tpl->tpl_vars['item'] = $__foreach_item_1_saved_local_item;
}
if ($__foreach_item_1_saved_item) {
$_smarty_tpl->tpl_vars['item'] = $__foreach_item_1_saved_item;
}
?>
                      </ul>
                  </div><!-- FECHA INFO E SUPORTE -->
              <?php
$_smarty_tpl->tpl_vars['link'] = $__foreach_link_0_saved_local_item;
}
if ($__foreach_link_0_saved_item) {
$_smarty_tpl->tpl_vars['link'] = $__foreach_link_0_saved_item;
}
?>	
                  </div>



            <div class="col-xs-4 col-sm-4">
                  <h3>Formas de Pagamento</h3>
                  <div class="pag">
                    <img src="http://www.static.artmilhas.com.br/www.artmilhas.com.br/default/images/payment.png" />

                  </div>
              </div>

        </div><!-- FECHA SUP ESQUERDA -->




                  </div><!-- FECHA ESQUERDA -->
                  <!-- ABRE DIREITA -->
                  <div class="col-md-4 col-sm-12">
                      <div>
                          <h3>Facebook</h3>
                          <div class="fb-page" data-href="https://www.facebook.com/artmilhas/" data-width="220" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false" data-show-posts="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/artmilhas/"><a href="https://www.facebook.com/artmilhas/">Artmilhas</a></blockquote></div></div>
                          <div id="fb-root"></div>
                          <?php echo '<script'; ?>
>
                            (function(d, s, id) {
                                var js, fjs = d.getElementsByTagName(s)[0];
                                if (d.getElementById(id)) return;
                                js = d.createElement(s); js.id = id;
                                js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5";
                                fjs.parentNode.insertBefore(js, fjs);
                              }(document, 'script', 'facebook-jssdk'));
                          <?php echo '</script'; ?>
>
                      </div>

                  </div><!-- FECHA DIREITA -->
              </div><!-- FECHA ROW -->
          </div><!-- FECHA CONTAINTER -->
<p class="bottom-text">
  © 2015 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['datastore']->value[0]->nome, ENT_QUOTES, 'UTF-8', true);?>
 – CNPJ: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['datastore']->value[0]->cnpj, ENT_QUOTES, 'UTF-8', true);?>
 -  Todos os direitos reservados.<br>
  Proibida reprodução total ou parcial. Preços e estoque sujeito a alterações sem aviso prévio. <br>
  Preços e condições válidas apenas para loja virtual.<br>
  <span>Endereço:</span> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['datastore']->value[0]->endereco, ENT_QUOTES, 'UTF-8', true);?>
, <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['datastore']->value[0]->numero, ENT_QUOTES, 'UTF-8', true);?>
 | CEP <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['datastore']->value[0]->cep, ENT_QUOTES, 'UTF-8', true);?>
, <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['datastore']->value[0]->bairro, ENT_QUOTES, 'UTF-8', true);?>
 - <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['datastore']->value[0]->cidade, ENT_QUOTES, 'UTF-8', true);?>
 / <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['datastore']->value[0]->estado, ENT_QUOTES, 'UTF-8', true);?>
<br>
  <span>Horário:</span> 2as às 6as-feiras: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['datastore']->value[0]->segunda_fisico, ENT_QUOTES, 'UTF-8', true);?>

</p>
  </div><!-- FECHA FOOTER BOX -->

  
  </footer>


</div><!-- FECHA CONTAINTER --><?php }
}
